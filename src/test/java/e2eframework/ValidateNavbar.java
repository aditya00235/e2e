package e2eframework;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import resources.Base;

public class ValidateNavbar extends Base
{
	public static Logger log=LogManager.getLogger(Base.class.getName());
	@BeforeTest
	public void initialize() throws IOException
	{
		driver=initializeDriver();
		driver.get(prop.getProperty("url"));
	}
	
	@Test
	public void validateNavbar() throws IOException
	{
		//driver=initializeDriver();
		//driver.get(prop.getProperty("url"));
		
		LandingPage landingpage=new LandingPage(driver);
		Assert.assertTrue(landingpage.getNavbar().isDisplayed());
		log.info("Displayed navigation bar");
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.close();
		driver=null;
	}
}
