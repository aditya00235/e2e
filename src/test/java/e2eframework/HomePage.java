package e2eframework;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import resources.Base;

public class HomePage extends Base
{
	public static Logger log=LogManager.getLogger(Base.class.getName());
	@BeforeTest
	public void initialize() throws IOException
	{
		log.info("Driver is initialized");
		driver=initializeDriver();
		log.info("Navigated to homepage");
		
	}
	
	@Test(dataProvider="getData")
	public void basePageNavigation(String username,String password) throws IOException
	{
		//driver=initializeDriver();
		driver.get(prop.getProperty("url"));
		
		LandingPage landing=new LandingPage(driver);
		landing.getLogin().click();
		
		LoginPage loginpage=new LoginPage(driver);
		loginpage.getUsername().sendKeys(username);
		loginpage.getpassword().sendKeys(password);
		loginpage.getloginbutton().click();
	}
	
	@DataProvider
	public Object[][] getData()
	{
		Object[][] data=new Object[1][2];
		data[0][0]="gooduser";
		data[0][1]="gooduserpassword";
		
		//data[1][0]="baduser";
		//data[1][1]="baduserpassword";
		
		return data;
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.close();
		driver=null;
	}
	
	
}
