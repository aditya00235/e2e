Feature: Login into Application

Scenario Outline: Positive test validation login
Given Initialize the browser with chrome
And Navigate to "http://www.qaclickacademy.com/" Site
And Click on Login link in home page to land on secure sign in Page
When User enters <username> and <password> and logs in
Then Verify that user is successfully logged in
And close browsers

Examples:
|username				|password		|
|aditya00@gmail.com		|adio12345		|
|sehwag@gmail.com		|546353728aa	|
|aman@gmail.com			|aman@123		|