package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage
{
	WebDriver driver;
	By login=By.cssSelector("a[href*='sign_in']");
	By title=By.xpath("(//div[@class='text-center'])[1]");
	By navbar=By.cssSelector(".navbar-collapse.collapse");
	By popsubscribe=By.xpath("//button[text()='NO THANKS']");
	public LandingPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getLogin()
	{
		return driver.findElement(login);
	}
	
	public WebElement getTitle()
	{
		return driver.findElement(title);
	}
	public List<WebElement> getpopSize()
	{
		return driver.findElements(popsubscribe);
	}
	public WebElement getpop()
	{
		return driver.findElement(popsubscribe);
	}
	public WebElement getNavbar()
	{
		return driver.findElement(navbar);
	}
}
