package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage
{
	WebDriver driver;
	By userName=By.xpath("//input[@id='user_email']");
	By password=By.xpath("//input[@id='user_password']");
	By loginButton=By.cssSelector("input[name='commit']");
	By popmsg=By.xpath("//div[@role='alert']");
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getUsername()
	{
		return driver.findElement(userName);
	}
	
	public WebElement getpassword()
	{
		return driver.findElement(password);
	}
	
	public WebElement getloginbutton()
	{
		return driver.findElement(loginButton);
	}
	public WebElement getpopmsg()
	{
		return driver.findElement(popmsg);
	}
}
